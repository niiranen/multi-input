CXX      := c++
CXXFLAGS := -pedantic-errors -Wall -Wextra -std=c++17 -fPIC -shared
LDFLAGS  := -L/usr/lib -lstdc++ -lm
INCLUDE  := -Iinclude

BUILD   := ./build
OBJ_DIR := $(BUILD)/obj
LIB_DIR := $(BUILD)/lib

SRC     := $(wildcard src/*.cpp) $(wildcard src/**/*.cpp)
OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)

TARGET := libmulti-input.so

.PHONY: clean all debug release

all: clean release

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -o $@ -c $<

$(LIB_DIR)/$(TARGET): $(LIB_DIR) $(OBJ_DIR) $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) $(LDFLAGS) -o $(LIB_DIR)/$(TARGET) $(OBJECTS)

$(LIB_DIR):
	@mkdir -p $(LIB_DIR)

$(OBJ_DIR):
	@mkdir -p $(OBJ_DIR)

debug: CXXFLAGS += -DDEBUG -g
debug: $(LIB_DIR)/$(TARGET)

release: CXXFLAGS += -O2
release: $(LIB_DIR)/$(TARGET)

clean:
	-@rm -rf $(BUILD)
