#ifndef INTERFACE_H
#define INTERFACE_H

#include "types.h"
#include <optional>

namespace MultiInput {
class Interface {
public:
  virtual ~Interface() = 0;

  /**
   * Pump the interface for new events.
   */
  virtual std::size_t pump() = 0;
  virtual std::optional<MultiInputEvent> poll() = 0;
};
} // namespace MultiInput

#endif // INTERFACE_H
