#ifndef X11_H
#define X11_H

#include "interface.h"
#include <X11/extensions/XInput2.h>

namespace MultiInput {
class X11Interface : public Interface {
public:
    X11Interface();
    ~X11Interface() override;

private:
    Display* display;
};
} // namespace MultiInput


#endif //X11_H
