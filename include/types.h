#ifndef MULTI_INPUT_TYPES_H
#define MULTI_INPUT_TYPES_H

#include <stdint.h>
#include <vector>

namespace MultiInput {
typedef uint32_t DeviceId;
typedef uint32_t Time;

typedef enum {
  DeviceConnect,
  DeviceDisconnect,
  RawMouseMove,
} EventType;

typedef struct {
  DeviceId source;
  Time time;
  EventType type;
  void *data;
} MultiInputEvent;

typedef struct {
  std::vector<int32_t> axisValues;
} RawMouseMoveEvent;
} // namespace MultiInput

#endif // MULTI_INPUT_TYPES_H
