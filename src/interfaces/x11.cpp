#include "interfaces/x11.h"

#include <X11/extensions/XInput2.h>

namespace MultiInput {
X11Interface::X11Interface() {
  display = XOpenDisplay(nullptr);
  if (display == nullptr) {
      throw std::runtime_error("Unable to get display");
  }

}
X11Interface::~X11Interface() {
    if (display != nullptr) {
        XCloseDisplay(display);
    }
}
}
